(defun PrintBoard(N state)
  (do ((i 0 (+ 1 i)))
    ((> i (- N 1)) 'done)
    (do ((j 0 (+ 1 j)))
    ((> j (- N 1)) (format t "~%"))
      (if (= j (setf temp (nth i state)))
        (format t "~A " temp)
        (format t "~A " '-)
        )
        )
      )
)

(defun UnderAttack (N state)
"Compute how many pairs of queens are under attack"
  (defun CheckAttack(row1 col1 row2 col2)
  (setf score 0)
  (if (or (= col1 col2) (= (+ row1 col1) (+ row2 col2)) (= (- row1 col1) (- row2 col2)))
      (setf score 1)
      )
      score
      )
(setf scores 0)
(do ((ii 0 (+ 1 ii)))
    ((> ii (- N 2)) 'done)
    (do ((jj (+ ii 1) (+ 1 jj)))
        ((> jj (- N 1)) 'done)
        (setf scores (+ scores (CheckAttack ii (nth ii state) jj (nth jj state))))
    )
  )
scores
)

(defun MinPosition (obj)
"Find the position of the minimum element in a list obj"
(setf N (length obj))
(setf minv (car obj))
(setf position 0)
(do ((ii 1 (+ 1 ii)))
    ((> ii (- N 1)) )
    (setf val (nth ii obj))
    (if (< val minv)
        (setf (values position minv) (values ii val))
      )
)
(setf ps (list position))
(do ((ii 0 (+ 1 ii)))
    ((> ii (- N 1)) )
    (if (= minv (nth ii obj))
        (push ii ps)
      )
  )
(setf ns (length ps))
(setf position (nth (random (- ns 1)) ps))
position
)
(defun Heuristic(N state pick)
"For a given pick(row), consider all possible columns.
For eahc column, compute the heuristic (how many queens
under attack (same colum, diagonals)).
Find the best position with the smallest heuristic.
Return the smallest heuristic and position"
  (setf h-scores (make-list N))
  (do ((i 0 (+ 1 i)))
    ((> i (- N 1)) 'done)
    (setf (nth i h-scores) (UnderAttack N (Update state pick i)))
  ) ; Compute the heurist for each case
  (setf p (MinPosition h-scores)) ; Find the position with the smallest heuristic
  (setf h (nth p h-scores)) ; Record the smallest heuristic

  (values h p) ; return h and p
)

(defun Update(state row newposition)
  (setf (nth row state) newposition)
  state
)

(defun SearchNQueens(N InitState)
  (setf MaxSteps 1000)
  (setf state InitState)
  (setf randomPick (random N))
  (setf (values h position) (Heuristic N state randomPick))
  (do ((step 1 (+ 1 step)))
      ((> step MaxSteps) (return (values state step)))
    (setf state (Update state randomPick position))
    (when (= h 0) (return (values state step)))
    (setf randomPick (random N))
    (setf (values h position) (Heuristic N state randomPick))
    )
)

(defun Nqueens(InitState)
"Solve N queens problem with initial state [s1,s2,...,sN],
where si means the ith queen is in the ith row and si-th column.
Notice that we already assume the N queens are in different rows,
but they may in the same column or diagonals.We use an informed
search algorithm to search ONE solution from the inital state. "
(setf N (length InitState)); N-problem size
(format t "----------~%")
(format t "Solve ~A x ~A Queens problem~%Initial input configuration~%" N N)
(PrintBoard N InitState) ; Print the inital NxN board
(format t "Working....~%")
(setf (values Sol steps) (SearchNQueens N InitState)) ; Search for a solution
(format t "Found a solution!~%
The number of nodes expanded  is ~A.~%The configuration is~%" (* N steps))
(PrintBoard N Sol); Print the solution
(format t "DONE!~%----------~%")
)
