# 8 Queens Puzzle

This is a programming assignment for 8 queens problem for CSCI 5511 at the University of Minnesota
This code works for N queens problem but may be slow.

Author: Shiqiang Xia

Time: 10/20/2019

Language :Lisp

## Algorithm 

Main idea: 6.4 Local search for CSPs (Artificial Intelligence: a mordern approach 3rd edition)

We assume the N queens are in different rows.

We use an informed search algorithm to search ONE solution from the inital state.

**heuristic how many queens under attack (same colum, diagonals)**



## How to run

`(load "8queens.lisp")`

`(Nqueens '(4 0 5 3 1 6 3 7))`


The input here is the column position of each queen on each row. 